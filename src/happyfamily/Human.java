package happyfamily;

import java.util.Random;

public class Human {

    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String schedule[][];

    void greetPet(){
        System.out.println("Привет, "+ pet.nickname+".");
    }

    void describePet(){
        String petTrickLevel;
        if (pet.trickLevel>50){
            petTrickLevel="очень хитрая";
        }
        else {
            petTrickLevel="почти не хитрая";
        }
        System.out.println("У меня есть "+pet.species+", ему "+pet.age+" лет, он "+petTrickLevel+".");
    }

    @Override
    public String toString(){
        return "Human{name='"+name+", surname='"+surname+", year="+year+", iq="+iq+", mother= "+String.join(" ",mother.name,mother.surname)+", father= "+String.join(" ",father.name,father.surname)+", pet= "+pet.toString();
    }
    boolean feedPet(boolean timeToEat){
        Random rndm= new Random();
        int random=rndm.nextInt(101);
        boolean feeding;
        if (timeToEat){
            System.out.println("Хм... покормлю ка я "+pet.nickname+".");
            feeding=true;
        }
        else {
            if (random<pet.trickLevel){
                System.out.println("Хм... покормлю ка я "+pet.nickname);
                feeding=true;

            }
            else{
                System.out.println("Думаю, "+pet.nickname+" не голодна.");
                feeding=false;
            }
        }
        return feeding;
    }

    Human(String name, String surname, int year){

        this.name=name;
        this.surname=surname;
        this.year=year;

    }

    Human(String name, String surname, int year, Human mother, Human father){

        this.name=name;
        this.surname=surname;
        this.year=year;
        this.mother=mother;
        this.father=father;

    }

    Human(String name, String surname, int year, Human mother, Human father, Pet pet, int iq, String schedule[][]){

        this.name=name;
        this.surname=surname;
        this.year=year;
        this.mother=mother;
        this.father=father;
        this.pet=pet;
        this.iq=iq;
        this.schedule=schedule;

    }

    Human(){

    }

}
