package happyfamily;

public class Main {
    public static void main(String[] args) {
    Pet bunny= new Pet();
    bunny.nickname= "Зая";
    bunny.species= "Кролик";
    bunny.trickLevel= 75;
    bunny.age=2;
    bunny.habits= new String[]{"Есть моркуву","Есть капусту"};

    Human katia= new Human();
    katia.name= "Екатерина";
    katia.surname= "Пупкина";
    Human oleg= new Human();
    oleg.name= "Олег";
    oleg.surname= "Пупкин";

    Human vasia=new Human();
    vasia.name= "Вася";
    vasia.surname= "Пупкин";
    vasia.year=1977;
    vasia.iq=80;
    vasia.pet=bunny;
    vasia.mother=katia;
    vasia.father=oleg;
    System.out.println(vasia.toString());


    Pet dog= new Pet("Пес","Гав");
    dog.trickLevel= 75;
    dog.age=2;
    dog.habits= new String[]{"Гавкать","Приносить тапки"};
    Human rita= new Human("Маргарита","Бабочкина",1953);
    Human evgen= new Human("Евгений","Бабочкин",1950);
    Human kolia= new Human("Николай","Бабочкин",1981);
    kolia.mother=rita;
    kolia.father=evgen;
    kolia.pet=dog;
    kolia.iq=99;
    System.out.println(kolia.toString());

    Pet cat1= new Pet("Кот","Кот",6,100,new String[]{"есть","Есть"});
    Human natasha=new Human("Наташа","Дудкина",1968);
    Human valera= new Human("Валера","Дудкин",1965);
    Human ira= new Human("Ира","Дудкина",1991, natasha, valera);
    ira.iq=60;
    ira.pet=cat1;
    System.out.println(ira.toString());

    Pet cat= new Pet("Кошка","Мурка",4,80,new String[]{"Спать","Играть"});
    Human victor= new Human("Віктор","Поп",1955);
    Human elena= new Human("Елена","Поп",1963);
    Human alla= new Human("Алла","Поп",1986,elena,victor,cat,70,new String[][]{{"Робота","Готовка"},{"Кино","Диван"}});
    System.out.println(alla.toString());
    }
}
